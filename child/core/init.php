<?php

class Child_Includes{

	public function init(){
		add_action('wp_enqueue_scripts', array($this, 'frontend_scripts'));
	}

	public function frontend_scripts(){
		wp_enqueue_script('child_script', C_THEME . '/core/js/child-script.js');
	}
};

$obj = new Child_Includes;
$obj->init();