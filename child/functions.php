<?php
define ('C_THEME', get_stylesheet_directory_uri() );
define ('C_CORE', get_stylesheet_directory() );


add_action('wp_enqueue_scripts', fn()=>	wp_enqueue_style('child_style', C_THEME . '/style.css', array('parent_style')));


require_once(C_CORE . '/core/init.php');
