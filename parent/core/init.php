<?php

class Parent_Includes {

	public function init(){
		 add_action('wp_enqueue_scripts', array($this, 'frontend_scripts'));
	}

	public function frontend_scripts(){
		wp_enqueue_script('parent_script', P_THEME . '/core/js/parent-script.js');
	}
};

$obj = new Parent_Includes;
$obj->init();