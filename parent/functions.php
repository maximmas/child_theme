<?php

define ('P_THEME', get_template_directory_uri() );
define ('P_CORE', get_template_directory() );


add_action('wp_enqueue_scripts', fn() => wp_enqueue_style('parent_style', P_THEME . '/style.css'));


require_once(P_CORE . '/core/init.php');


