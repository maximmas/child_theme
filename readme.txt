Исходные данные

Есть WP-тема 'Parent', в которой подключается js-скрипт.
Подключение происходит в классе Parent_Includes ( в методе init() ),  
который находится в файле core/init.php и подключается 
в functions.php через require_once

Обычно для публичных тем разработчики добавляют в классы возможность их переопределения, т.е. 
перед объявлением класса стоит проверка: 
if( !class_exists('My_Class')){
	class My_Class{
	 	....
	}
}
В этом случае можно создать дочернюю тему и в ней определить такой класс и в нем 
изменить что нужно. Дочка загружается первой, объявляет класс, родитель видит что класс существует и 
не объявляет свой. Все ок. 

В данном случае, в которой нет такой проверки.

Но т.к. это премиальная тема, должна быть возможность ее обновлять, т.е. нельзя вручную править 
код данной темы. 

Задача: 
Сделать так, что бы тема 'Parent' не загружала js-файл core/js/parent-script.js
Вместо него дочерняя тема 'Child' должна загружать свой файл core/js/child-script.js
Я сделал небольшую упрощенную модель, в ней текст должен быть красным. 

Как сделать ? И возможно ли 
Переопределить в дочке класс нельзя.  
Сделать в дочке свой класс Child_includes, 
который бы расширял родительский Parent_Includes и переопределял родительский метод init ? 
Но дочка загружается первой и на момент ее загрузки Parent_Includes еще не объявлен, это тоже ошибка.

